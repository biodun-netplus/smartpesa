package com.netplus.smartpesa.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netplus.smartpesa.entity.SmartPesa;
import com.netplus.smartpesa.repos.SmartPesaRepository;
import com.netplus.smartpesa.service.MerchantNotifyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static java.lang.Math.toIntExact;

@Slf4j
@Api(value="NETPOS Endpoint", description="Query transactions notifications")
@RestController
@RequestMapping("/v1/pos")
public class SmartPesaEndpoint {

    @Autowired
    private SmartPesaRepository smartPesaRepository;
    @Autowired
    private MerchantNotifyService merchantNotifyService;

    @PostMapping
    public ResponseEntity<String> notification(@RequestBody SmartPesa smartPesa) throws ExecutionException, InterruptedException {
        log.info("call notification");
        log.info(smartPesa.toString());
        smartPesaRepository.save(smartPesa);

        merchantNotifyService.pushTransaction(smartPesa);

        return new ResponseEntity<>("ok", HttpStatus.OK);
    }


    @ApiOperation(value = "Query a list of trasactions by date and MID assigned to you", response = List.class)
    @GetMapping(path = "/notifications")
    public Page<SmartPesa> fetchNotifications(
            @RequestParam String mid,
            @RequestParam(value="startTime") @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") Date startTime,
            @RequestParam(value="endTime") @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") Date endTime,
            Pageable pageable){

        return smartPesaRepository.findSmartPesaByMidAndTransaction_datetimeIsBetween(mid, startTime, endTime, pageable);
    }
}
