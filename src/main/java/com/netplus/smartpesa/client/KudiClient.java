package com.netplus.smartpesa.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Map;

@Component
@FeignClient(name = "kudi", url = "https://pollux.kudi.ai", fallback = KudiClient.KudiClientImpl.class)
public interface KudiClient {

    @PostMapping("/callbacks/netplus/notifications")
    //@Headers("bearer: ${kudi.push.token}")
    ResponseEntity<Object> push(@RequestHeader("bearer") String token, @RequestBody Map map);
    //ResponseEntity<Object> push(@HeaderMap MultiValueMap<String, String> headers, @RequestBody Map map);
    //ResponseEntity<Object> push(@Param("token") String token, @RequestBody Map map);

    @Component
    @Slf4j
    class KudiClientImpl implements KudiClient {

        @Override
        public ResponseEntity<Object> push(String token, Map map) {
            log.info(" got into fail over");
            return null;
        }
    }
}
