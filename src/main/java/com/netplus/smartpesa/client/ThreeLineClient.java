package com.netplus.smartpesa.client;

import com.netplus.smartpesa.pojo.PushRequest;
import com.netplus.smartpesa.pojo.PushResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(name = "ThreeLine", url = "${3line.push.endpoint}", fallback = ThreeLineClient.ThreeLineClientImpl.class)
public interface ThreeLineClient {

    @PostMapping
    PushResponse push(@RequestBody PushRequest request);

    @Component
    @Slf4j
    class ThreeLineClientImpl implements ThreeLineClient {

        @Override
        public PushResponse push(PushRequest request) {
            log.info(" got into fail over");
            return null;
        }
    }
}
