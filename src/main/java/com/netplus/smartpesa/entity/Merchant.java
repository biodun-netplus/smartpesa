package com.netplus.smartpesa.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
@Table(name = "POS_MER_ENDPOINT")
public class Merchant implements Serializable {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "M_ID")
    private String mid;
    @Column(name = "END_POINT")
    private String endPoint;
    @Column(name = "APP_ID")
    private String appId;
    @Column(name = "APP_KEY")
    private String appKey;
    @Column(name = "ENABLED")
    private Boolean enabled = false;
}
