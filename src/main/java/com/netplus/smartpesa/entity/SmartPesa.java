package com.netplus.smartpesa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "POS_TRANS")
//@JsonIgnoreProperties(ignoreUnknown=true)
public class SmartPesa implements Serializable {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String merchant_name;
    private String merchant_id;
    private String serial_number;
    private String terminal_id;
    private String message_type;
    private String acquiring_bank_id;
    private String operator_id;
    private String transaction_type;
    private Double transaction_processing_fee;
    private String device_id;
    private String external_reference;
    private String transaction_id;
    private Date transaction_datetime = new Date();
    private String transaction_reference;
    private Integer transaction_type_enum_id;
    private Double amount;
    private String authorisation_id;
    private String card_name;
    private String card_number;
    private String currency_id;
    private String currency_symbol;
    private String response_code;
    private String retrieval_reference_number;
    @JsonIgnore
    private int reversed;
    private String system_trace_audit_number;
    private String tid;
    private String mid;
    private String app_label;
    private String acquiring_bank_name;
    private String response_description;
    private Double tip;
    private Double transaction_fee;
    private Double convenience_fee;
    private Double referral_fee;


    /** to be deleted**/
//    private String operator_name;
//    private String operator_code;
//    private String merchant_code;
//    private String authorisation_response;
//    private String authorisation_response_code;
//    private String card_type;
//    private String card_brand;
//    private Double cashback = 0.00d;
//    private String currency_name;
//    private String currency_iso4217;
//
//
//    private String settle_response_code;
//    private String settled_status;
//
//    private String post_processing_id;
//    private String aid;
//
//    private Double tip;
//    private Double transaction_fee;
//    private Double convenience_fee;
//    private Double referral_fee;
}
/*
{"merchant_name":"ETRANZACT INT PLC",
"merchant_id":"65e4b74b-0dc8-8e22-668b-5b5a640ce9a6","serial_number":"22160342","terminal_id":"8d12946e-bac8-1c99-ad9c-5b6c651ffe71",
"message_type":"0200","acquiring_bank_id":"518855","operator_id":"6f097aca-5d9e-2d57-ba32-5b6c652a80bb","transaction_type":"00","transaction_processing_fee":0.0,
"device_id":"23f0c63d-c036-7634-b65d-5b912e4ebd65","external_reference":null,"transaction_id":"8a662125-e8ea-4c14-893d-aa0100bf296a","transaction_datetime":"2019-02-27T11:35:59Z",
"transaction_reference":"NETx70455","transaction_type_enum_id":1,"amount":15000.0,"authorisation_id":"275590","card_name":"ADEBOWALE/TAJUDEEN",
"card_number":"5399 83XX XXXX 2641","currency_id":"6d02d256-4a89-41e7-90c1-ef93eee9cdd6","currency_symbol":566,"response_code":0,"retrieval_reference_number":"70455",
"reversed":false,"system_trace_audit_number":149090,"tid":"2044XU46","mid":"2044LA000020466","app_label":"Debit MasterCard","tip":0.0,"transaction_fee":0.0,"convenience_fee":0.0,"referral_fee":0.0
 */