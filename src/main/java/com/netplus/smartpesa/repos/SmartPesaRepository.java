package com.netplus.smartpesa.repos;

import com.netplus.smartpesa.entity.SmartPesa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface SmartPesaRepository extends JpaRepository<SmartPesa, Integer> {


    @Query("select s from SmartPesa s where s.transaction_reference = :transactionRef")
    List<SmartPesa> findSmartPesaByTransaction_reference(@Param("transactionRef") String transactionRef);

    @Query("select s from SmartPesa s where s.mid = :mid and s.transaction_datetime >= :startDate and s.transaction_datetime <= :endDate")
    Page<SmartPesa> findSmartPesaByMidAndTransaction_datetimeIsBetween(@Param("mid") String mid, @Param("startDate") Date endDate, @Param("endDate") Date startDate, Pageable pageable);
}
