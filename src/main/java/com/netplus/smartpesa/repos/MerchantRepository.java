package com.netplus.smartpesa.repos;

import com.netplus.smartpesa.entity.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MerchantRepository extends JpaRepository<Merchant, Integer> {

    @Query("select m from Merchant m where m.enabled=:enabled")
    List<Merchant> findAllMerchantByEnabled(@Param("enabled") Boolean enabled);

}
