package com.netplus.smartpesa.service;


import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netplus.smartpesa.entity.SmartPesa;
import com.netplus.smartpesa.pojo.PushJson;
import com.netplus.smartpesa.pojo.PushRequest;
import com.netplus.smartpesa.pojo.PushResponse;
import com.netplus.smartpesa.repos.SmartPesaRepository;
import com.netplus.smartpesa.util.AES;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class MerchantNotifyService {

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private SmartPesaRepository smartPesaRepository;

    @Value(value = "${kudi.push.token}")
    private String kudiToken;
    @Value(value = "${kudi.push.miid}")
    private String kudiMiid;
    @Value(value = "${kudi.push.endpoint}")
    private String kudiEndPoint;


    @Value(value = "${cellulant.push.miid}")
    private String cellulantMiid;
    @Value(value = "${cellulant.push.production.url.notification}")
    private String cellulantEndPoint;

    @Value(value = "210000000000221")
    private String payGoMiid;
    @Value(value = "https://paygo.payant.ng:8006/pos/netpos-notification")
    private String payGoEndPoint;

    @Value(value = "${hackman.push.production.url.token}")
    private String hackmanTokenEndpoint;
    @Value(value = "${hackman.push.miid}")
    private String hankManMiid;
    @Value(value = "${hackman.push.terminalid}")
    private String hankManTerminalId;
    @Value(value = "${hackman.push.production.url.notification}")
    private String hackmanEndPoint;
    @Value(value = "${hackman.push.clientid}")
    private String hackmanClientId;
    @Value(value = "${hackman.push.clientsecret}")
    private String hackmanClientSecret;

    @Value(value = "${3line.push.miid}")
    private String threeLineMiid;
    @Value(value = "${3line.push.key}")
    private String threeLineKey;

    @Value(value = "${3line.push.endpoint}")
    private String threeLineEndPoint;





//    @Bean
//    public RestTemplate restTemplate() {
//        return new RestTemplate();
//    }


      public String pushTransaction(SmartPesa smartPesa) throws ExecutionException, InterruptedException {
        log.info("Looking up " + smartPesa);


        //log.info("Checking if Kudi "  + smartPesa.getMerchant_name());
        //log.info("Is HACKMAN MFB LTD " + smartPesa.getMerchant_name().equalsIgnoreCase("HACKMAN MFB LTD"));

        if (smartPesa.getMerchant_name().equalsIgnoreCase("Kudi") ) {
            log.info("for kudi integration");

            log.info("endpoint [" + kudiEndPoint + "] for mid [" + smartPesa.getMid() + "]");


                Map map = new HashMap<>();
                map.put("terminalId", smartPesa.getTerminal_id());
                map.put("amount", smartPesa.getAmount() * 100d);
               // map.put("statusCode", smartPesa.getResponse_code());
                if (smartPesa.getMessage_type().equalsIgnoreCase("0200")) {
                    map.put("statusCode", "00");
                    map.put("statusDescription", "Approved");
                }
                else if (smartPesa.getMessage_type().equalsIgnoreCase("0420")) {
                    map.put("statusCode", smartPesa.getMessage_type());
                    map.put("statusDescription", "Reversed");
                }
                else {
                    map.put("statusCode", smartPesa.getMessage_type());
                    map.put("statusDescription", "Failed");
                }
                map.put("pan", smartPesa.getCard_number());
                map.put("rrn", smartPesa.getRetrieval_reference_number());
                map.put("transactionType", smartPesa.getTransaction_type());
                map.put("cardName", smartPesa.getCard_name());
                map.put("transactionTime", DateFormatUtils.format(smartPesa.getTransaction_datetime(), "yyyy-MM-dd HH:mm:ss"));
                map.put("reference", smartPesa.getTransaction_reference());
                map.put("longitude", "10.0");
                map.put("latitude", "12.0");

                log.info(" map {} ", map);
                log.info(" token {} ", kudiToken);

                ObjectMapper mapper = new ObjectMapper();
            String jsonString = null;
            try {
                jsonString = mapper.writeValueAsString(map);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            log.info("jsonString = " + jsonString);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                //headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
                headers.add("Authorization", "Bearer " + kudiToken);
                //headers.setBearerAuth(kudiToken);

                HttpEntity<String> entity = new HttpEntity<>(jsonString, headers);

                ResponseEntity<String> response = restTemplate.exchange(kudiEndPoint, HttpMethod.POST, entity, String.class);

                log.info("Push response status code: " + response.getStatusCode());
        }

        if (smartPesa.getMid().equalsIgnoreCase(hankManMiid) || smartPesa.getMerchant_name().equalsIgnoreCase("HACKMAN MFB LTD")){
            log.info("for Hackman integration");

            log.info("endpoint [" + hackmanTokenEndpoint + "] for mid [" + smartPesa.getMid() + "]");
            log.info(hackmanEndPoint);
            final SmartPesa smartPesaModified = smartPesa;
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                // Simulate a long-running Job
                try {
                    TimeUnit.SECONDS.sleep(130);
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                }

                List<SmartPesa> smartPesaList = smartPesaRepository.findSmartPesaByTransaction_reference(smartPesaModified.getTransaction_reference());
                SmartPesa smartPesa1 = smartPesaModified;
                log.info("Size "+ smartPesaList.size());
                if (smartPesaList.size() > 1){
                    smartPesa1 = smartPesaList.get(1);
                    if (smartPesa1.getReversed() == 1)
                        return;
                    smartPesa1.setReversed(1);
                    smartPesaRepository.save(smartPesa1);

                }
                final SmartPesa smartPesa2 = smartPesa1;
                 final Map map = new HashMap<>();
                map.put("client_id", hackmanClientId);
                map.put("client_secret", hackmanClientSecret);

                log.info(" map {} ", map);
                final ObjectMapper mapper = new ObjectMapper();
                String jsonString = null;
                try {
                    jsonString = mapper.writeValueAsString(map);
                } catch (JsonProcessingException e) {
                    log.info(" error {} ", e.getMessage());
                }
                log.info("jsonString = " + jsonString);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                HttpEntity<String> entity = new HttpEntity<>(jsonString, headers);
               // ResponseEntity<String> response = restTemplate.exchange(hackmanTokenEndpoint, HttpMethod.POST, entity, String.class);
                /*String token = response.getBody();
                log.info(" token {} ", token);
                HashMap<String, Object> map2 = new HashMap<String, Object>();*/

                AsyncRestTemplate asyncRestTemplate = new AsyncRestTemplate(new ConcurrentTaskExecutor(Executors.newFixedThreadPool(100)));

                asyncRestTemplate.exchange(hackmanTokenEndpoint, HttpMethod.POST, entity, String.class)
                        .addCallback(new ListenableFutureCallback<ResponseEntity<String>>() {
                            @Override
                            public void onSuccess(ResponseEntity<String> result) {
                                //TODO: Add real response handling
                                String token = result.getBody();
                                log.info(" token {} ", token);
                                HashMap<String, Object> map2 = new HashMap<String, Object>();
                                try
                                {
                                    //Convert Map to JSON
                                    map2= mapper.readValue(token, new TypeReference<Map<String, Object>>(){});

                                    //Print JSON output
                                    log.info(map2.toString());
                                }
                                catch (JsonGenerationException e) {
                                    e.printStackTrace();
                                } catch (JsonMappingException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                String accessToken = map2.get("access_token").toString();
                                log.info("Access Token {} ", accessToken);
                                try {
                                Map map3 = new HashMap<>();
                                //map = new HashMap<>();
                                map3.put("Reference", "null");
                                map3.put("Amount", smartPesa2.getAmount());
                                map3.put("Currency", smartPesa2.getCurrency_symbol());
                                map3.put("Type", "Purchase");
                                map3.put("CardScheme", "null");
                                if (smartPesa2.getMessage_type().equalsIgnoreCase("0200")) {
                                    map3.put("StatusCode", "00");
                                    map3.put("StatusDescription", "Approved");
                                }
                                else if (smartPesa2.getMessage_type().equalsIgnoreCase("0420")) {
                                    map3.put("StatusCode", smartPesa2.getMessage_type());
                                    map3.put("StatusDescription", "Reversed");
                                }
                                else {
                                    map3.put("StatusCode", smartPesa2.getMessage_type());
                                    map3.put("StatusDescription", "Failed");
                                }
                                map3.put("CustomerName", smartPesa2.getCard_name());
                                map3.put("PaymentDate", DateFormatUtils.format(smartPesa2.getTransaction_datetime(), "yyyy-MM-dd HH:mm:ss"));

                                map3.put("RetrievalReferenceNumber", smartPesa2.getRetrieval_reference_number());
                                map3.put("TransactionReference", smartPesa2.getTransaction_id());

                               // log.info(" map {} ", map3.toString());

                                ObjectMapper mapper3 = new ObjectMapper();


                                final String jsonString = mapper3.writeValueAsString(map3);

                                log.info("jsonString = " + jsonString);

                                HttpHeaders headers3 = new HttpHeaders();
                                headers3.setContentType(MediaType.APPLICATION_JSON);
                                //headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                                log.info(" token {} ", accessToken);
                                headers3.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
                                headers3.add("Authorization", "Bearer " + accessToken);
                                headers3.add("TerminalID", smartPesa2.getTid());
                                headers3.add("Accept", "application/json");

                                HttpEntity<String> entity3 = new HttpEntity<>(jsonString, headers3);
                                // RestTemplate restTemplate2 = new RestTemplate();

                                    ResponseEntity<String> response = restTemplate.exchange(hackmanEndPoint, HttpMethod.POST, entity3, String.class);
                                    String body = response.getBody();
                                    log.info(" response {} ", body);
                                    log.info("Push response status code: {} " , response.getStatusCodeValue());
                                }
                                catch (Exception e){
                                    log.error(e.getMessage());
                                    e.printStackTrace();
                                }


                            }

                            @Override
                            public void onFailure(Throwable ex) {
                                //TODO: Add real logging solution

                                log.error(ex.getMessage());
                            }
                        });


            });
            // Block and wait for the future to complete
            //future.get();

        }

          if (smartPesa.getMerchant_name().equalsIgnoreCase("Cellulant") || smartPesa.getMid().equalsIgnoreCase(cellulantMiid)) {
              log.info("for Cellulant integration");

              log.info("endpoint [" + cellulantEndPoint + "] for mid [" + smartPesa.getMid() + "]");
              String timeStamp = "";
              try {
                  timeStamp = "" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(DateFormatUtils.format(smartPesa.getTransaction_datetime(), "yyyy-MM-dd HH:mm:ss")).getTime();
              } catch (ParseException e) {
                  e.printStackTrace();
              }

              Double amount = smartPesa.getAmount() * 100;


              String textToHash = smartPesa.getTerminal_id().toString() + smartPesa.getTransaction_reference().toString() +
                      "Purchase" + amount.toString() + timeStamp.toString() + "CPOSPayment" + "73h23ww8733hn";

              UriComponentsBuilder builder = UriComponentsBuilder
                      .fromUriString(cellulantEndPoint)
                      // Add query parameter
                      .queryParam("TerminalId", smartPesa.getTid().toString())
                      .queryParam("TxnRef", smartPesa.getTransaction_reference().toString())
                      .queryParam("TxnType", "Purchase")
                      .queryParam("Amount", amount.toString())
                      .queryParam("Timestamp", timeStamp.toString())
                      .queryParam("Narration", "CPOSPayment")
                      .queryParam("PartnerREF", "TG001")
                      .queryParam("Hash", hash512(textToHash, "SHA-512"));

              log.info(" textTohash {} ", textToHash);
              log.info(" hashValue {} ", hash512(textToHash, "SHA-512"));
              log.info(" endpoint {} ", builder.toUriString());

              HttpHeaders headers = new HttpHeaders();
              headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
              HttpEntity<String> entity = new HttpEntity<>(headers);

              ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, String.class);

              log.info("Push response status code: " + response.getStatusCode());
              log.info("Push response body: " + response.getBody());
              log.info("Push response status code: " + response.getStatusCodeValue());
          }


            if (smartPesa.getMerchant_name().equalsIgnoreCase("3 LINE") || smartPesa.getMid().equalsIgnoreCase(threeLineMiid)) {
//                String endPoint = StringUtils.replaceIgnoreCase(merchant.getEndPoint(), "{appId}", threeLineAppId);
                log.info("endpoint [" + threeLineEndPoint + "] for mid ["+ smartPesa.getMid() +"]");
                try {

                    PushJson pushJson = new PushJson();
                    pushJson.setAmount(smartPesa.getAmount() * 100);
                    pushJson.setTerminalId(smartPesa.getTid());
                    if (smartPesa.getMessage_type().equalsIgnoreCase("0200")) {
                        pushJson.setStatusCode("00");
                    }
                    else if (smartPesa.getMessage_type().equalsIgnoreCase("0420")) {
                        pushJson.setStatusCode("09");
                    }
                    else {
                        pushJson.setStatusCode("00");
                    }
                   // pushJson.setStatusCode(smartPesa.getResponse_code());
                    pushJson.setPan(smartPesa.getCard_number());
                    pushJson.setRrn("0"+smartPesa.getRetrieval_reference_number());
                    pushJson.setReversal(false);
                    pushJson.setStan(smartPesa.getSystem_trace_audit_number());
                    pushJson.setBank(smartPesa.getAcquiring_bank_name());
                    pushJson.setTransactionType(smartPesa.getMerchant_name());
                    pushJson.setProductId(smartPesa.getMerchant_name());
                    pushJson.setTransactionTime(DateFormatUtils.format(smartPesa.getTransaction_datetime(), "yyyy-MM-dd HH:mm:ss"));

                    ObjectMapper mapper = new ObjectMapper();
                    String jsonString = mapper.writeValueAsString(pushJson);
                    log.info("jsonString = " + jsonString);

                    // Encryption .....
                    PushRequest request = new PushRequest();
                    request.setRequest(AES.doEncrpt(jsonString, threeLineKey));

//                    log.info("Push request: " + request);
//
//                    PushResponse response = threeLineClient.push(request);



                    HttpHeaders headers = new HttpHeaders();
                    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

                    HttpEntity<PushRequest> entity = new HttpEntity<>(request, headers);

                    ResponseEntity<PushResponse> response = restTemplate.exchange(threeLineEndPoint, HttpMethod.POST, entity, PushResponse.class);
                    log.info("Push response status code: " + response.getStatusCode());
                    log.info("Push response body: " + response.getBody());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            if(smartPesa.getMerchant_name().equalsIgnoreCase("Kabir Shittu") || smartPesa.getMid().equalsIgnoreCase(payGoMiid)){
                log.info("For Pay Go Integration");
                log.info("endpoint [" + payGoEndPoint + "] for mid [" + smartPesa.getMid() + "]");
                log.info("statusCode [" + smartPesa.getMessage_type().toString() + "]");
                log.info("TerminalID [" +smartPesa.getTerminal_id() + "]");
                Map map = new HashMap<>();
                map.put("terminalId", smartPesa.getTid());
                map.put("amount", smartPesa.getAmount());
                // map.put("statusCode", smartPesa.getResponse_code());
                if (smartPesa.getMessage_type().equalsIgnoreCase("0200")) {
                    map.put("statusCode", "00");
                    map.put("statusDescription", "Approved");
                }
                else if (smartPesa.getMessage_type().equalsIgnoreCase("0420")) {
                    map.put("statusCode", smartPesa.getMessage_type());
                    map.put("statusDescription", "Reversed");
                }
                else {
                    map.put("statusCode", smartPesa.getMessage_type());
                    map.put("statusDescription", "Failed");
                }
                map.put("pan", smartPesa.getCard_number());
                map.put("rrn", smartPesa.getRetrieval_reference_number());
                map.put("transactionType", smartPesa.getTransaction_type());
                map.put("cardName", smartPesa.getCard_name());
                map.put("transactionTime", DateFormatUtils.format(smartPesa.getTransaction_datetime(), "yyyy-MM-dd HH:mm:ss"));
                map.put("reference", smartPesa.getTransaction_reference());
                map.put("longitude", "10.0");
                map.put("latitude", "12.0");


                log.info(" map {} ", map);

                ObjectMapper mapper = new ObjectMapper();
                String jsonString = null;
                try {
                    jsonString = mapper.writeValueAsString(map);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                log.info("jsonString = " + jsonString);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                //headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
                //headers.add("Authorization", "Bearer " + kudiToken);
                //headers.setBearerAuth(kudiToken);

                HttpEntity<String> entity = new HttpEntity<>(jsonString, headers);


                ResponseEntity<String> response = restTemplate.exchange(payGoEndPoint, HttpMethod.POST, entity, String.class);

                log.info("Push response status code: " + response.getStatusCode());
                //log.info("Push response status code: " + response);

                //log.info("Push response status code: " + response.getStatusCode());
               log.info("Push response body: " + response.getBody());

            }

        return smartPesa.toString();
    }

    public static String hash512(String hash_verify, String alg) {
        String hash = null;
        try {
            MessageDigest md = MessageDigest.getInstance(alg);
            md.update(hash_verify.getBytes());
            byte[] byteData = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; ++i) {
                sb.append(Integer.toString((byteData[i] & 255) + 256, 16).substring(1));
            }
            System.out.println("Hex format : " + sb.toString());
            hash = sb.toString();
        }
        catch (NoSuchAlgorithmException ex) {
           ex.printStackTrace();
        }
        return hash;
    }
}
/*
"terminalId": "25125678", //Terminal ID (String)
"amount": 5000, //Amount in kobo (bigInt)
"statusCode": "00", //Transaction Status Code (String)
"pan": "5399********6892", //Card PAN (String)
"rrn": "AKRN00000007", //Unique reference number (String)
"transactionType": "KUDI", //Transaction Type if available (String)
"cardName": "Cardholder Name", //Card Holder name if available (String)
"transactionTime": "2018-04-10 09:00:00" //Transaction Time Y-m-d H:i:s (datetime)
"reference": "unique_netplus_reference",
"longitude": "long",
"latitude": "lat"
 */