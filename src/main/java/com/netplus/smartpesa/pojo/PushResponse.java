package com.netplus.smartpesa.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class PushResponse implements Serializable {
    private String respDesc;
    private String respCode;
}
