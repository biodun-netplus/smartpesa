package com.netplus.smartpesa.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class PushJson implements Serializable {

    private Double amount;
    private String terminalId;
    private String statusCode;
    private String pan;
    private String rrn;
    private Boolean reversal;
    private String stan;
    private String bank;
    private String transactionType;
    private String productId;
    private String transactionTime;

    public PushJson(Double amount, String terminalId, String statusCode, String pan, String rrn, Boolean reversal, String stan, String bank, String transactionType, String productId, String transactionTime) {
        this.amount = amount;
        this.terminalId = terminalId;
        this.statusCode = statusCode;
        this.pan = pan;
        this.rrn = rrn;
        this.reversal = reversal;
        this.stan = stan;
        this.bank = bank;
        this.transactionType = transactionType;
        this.productId = productId;
        this.transactionTime = transactionTime;
    }
}
