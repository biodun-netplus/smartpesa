package com.netplus.smartpesa.util;

import com.netplus.smartpesa.entity.Merchant;
import com.netplus.smartpesa.repos.MerchantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;
import java.util.List;

@Component
public class DataLoader implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(DataLoader.class);
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private ServletContext servletContext;

    @Override
    public void run(ApplicationArguments args)  {
        try {
            logger.info("loading initiate parameters");
            //Map<String, String> map = new HashMap<>();

            List<Merchant> merchantList = merchantRepository.findAllMerchantByEnabled(true);

           // merchantList.forEach(merchant -> map.put(merchant.getMid(), merchant.getEndPoint()));

            servletContext.setAttribute("pushMerchants", merchantList);
//            logger.info("total merchant [" + CollectionUtils.size(merchantList) +"]");
//            logger.info("total merchant [" + CollectionUtils.size(map) +"]");
           logger.info("total merchant [" + merchantList +"]");

            logger.info("After loading initiate parameters");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
