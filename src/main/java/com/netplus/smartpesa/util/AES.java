package com.netplus.smartpesa.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class AES {

    public static String doEncrpt(String data, String appKey) {

        try {
            byte [] key = Hex.decodeHex(appKey.toCharArray());

            Cipher cipher = Cipher.getInstance("AES");

            SecretKey originalKey = new SecretKeySpec(key, 0, key.length, "AES");

            cipher.init(Cipher.ENCRYPT_MODE, originalKey);

            return Base64.encodeBase64String(cipher.doFinal(data.getBytes("UTF-8")));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
