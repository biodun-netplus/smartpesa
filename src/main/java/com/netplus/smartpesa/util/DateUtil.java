package com.netplus.smartpesa.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;

@Slf4j
public class DateUtil {

    public static void main(String... args) {
        log.info("Date format {} ", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
    }
}
