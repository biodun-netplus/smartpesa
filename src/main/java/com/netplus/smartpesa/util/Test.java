package com.netplus.smartpesa.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Test {
    public static void main(String... args) {
        //
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        System.out.println(dt.format(new Date()));

        //SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //2018-12-31 11:52:33

    }
}
